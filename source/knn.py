import pandas as pd;
import nltk
from sklearn.neighbors import KNeighborsClassifier
from sklearn.model_selection import cross_val_score
from sklearn import datasets

def train(df, fold, num, col):
    data = df.drop(columns = col);
    label = df[col];

    featuresets = [data.loc[index] for index in df.index]#puts all into matrix. List of list
    cutoff = int(len(featuresets)*fold);
    train_set, train_label = featuresets[:cutoff], label[:cutoff]#get's some to train
    
    X = train_set
    y = train_label
    neigh = KNeighborsClassifier(n_neighbors=num)
    neigh.fit(X, y);
#    print(neigh.score(X, y));#just seeing how well it fits self
    return neigh;
    
def test(df, fold, model, col):
    data = df.drop(columns = col);
    label = df[col];

    featuresets = [data.loc[index] for index in df.index]#puts all into matrix. List of list
    cutoff = int(len(featuresets)*fold);
    test_set, test_label = featuresets[cutoff:], label[cutoff:]#get's some to train
    
    X = test_set
    y = test_label
    score = model.score(X, y)
    
    column = {};
    return score, test_label, test_set;
    
def print_informative_features(df, model, test_label, test_set):
    print('Most Informative Feature  Accuracy')
    column = {}
    X = test_set
    y = test_label
    for i in range(len(X[0])):#goes through column
        X = [[row[i]] for row in test_set]#get's first columns from target
        scores = cross_val_score(model, X, y, cv = 3)#get's column value score
        column[scores.mean()]= df.columns[i];
    
    for score in sorted(column, reverse=True)[:10]:
        print(column[score]+" "+str(score));

    print();return score;

def percentage_popular(df, col):
    label = df[col];
    pop, nonpop = 0, 0;
    for index in label:
        if(index==1):
            pop+=1;
        elif(index==0):
            nonpop+=1;
        else:
            print("something wrong")
    print("Popular percentage is: "+str(pop/len(label)*100));
    print("Unpopular percentage is: "+str(nonpop/len(label)*100));
    
def main():    
    col = input("Enter \"views\" or \"comments\": ");
    df = pd.read_csv("features_"+col+".csv")
    df.drop(df.columns[df.columns.str.contains('unnamed',case = False)],axis = 1, inplace = True)

    percentage_popular(df, col);
    for per in range(4, 9):
        total = 0;
        for i in range(0, 10):
            df = df.sample(frac=1).reset_index(drop=True)
            model = train(df, .1*per, 1, col);
            triple = test(df, .1*per, model, col);
            total+=triple[0];
            test_label, test_set = triple[1], triple[2]
        print("Classifier Accuracy for "+str(per*.1)+": ");
        print(total/10);

    print_informative_features(df, model, test_label, test_set);

if __name__== "__main__":
  main()
