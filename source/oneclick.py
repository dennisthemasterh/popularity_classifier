import combine
import load_and_clean
import naive_bayes
import gaussian_NB
import knn
import baseline

def main():
    print("running workflow")
    combine.main();
    load_and_clean.main();
    naive_bayes.py.main();
    gaussian_NB.py.main();
    knn.py.main();
    baseline.py.main();

if __name__ == "__main__":
    main()