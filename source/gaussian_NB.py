import nltk
from sklearn.naive_bayes import GaussianNB
import pandas as pd
from sklearn.model_selection import cross_val_score
import warnings

warnings.filterwarnings("ignore")
        
def train(df, fold, col):
    data = df.drop(columns = col);
    label = df[col];

    featuresets = data.copy(deep=True);
    cutoff = int(len(featuresets)*fold);
    
    train_set, train_label = featuresets[:cutoff], label[:cutoff]
    test_set, test_label = featuresets[cutoff:], label[cutoff:]

    clf = GaussianNB()
    clf.fit(train_set, train_label)
    return clf;

def test(df, fold, clf, col):
    data = df.drop(columns = col);
    label = df[col];
    featuresets = data.copy(deep=True);
    cutoff = int(len(featuresets)*fold);

    test_set, test_label = featuresets[cutoff:], label[cutoff:]

    return clf.score(test_set, test_label), test_label, test_set;

def print_informative_features(df, clf, test_label, test_set):
    print('Most Informative Feature  Accuracy')
    column = {}

    for i in range(len(test_set.columns)):#goes through column
        X = [[test_set.loc[row].iloc[i]] for row in test_set.index]#get's first columns from target
        scores = cross_val_score(clf, X, test_label, cv = 3)#get's column value score
        column[scores.mean()]= df.columns[i];
    
    for score in sorted(column, reverse=True)[:15]:
        print(column[score]+" "+str(score));
    print();return score;

def percentage_popular(df, col):
    label = df[col];
    pop, nonpop = 0, 0;
    for index in label:
        if(index==1):
            pop+=1;
        elif(index==0):
            nonpop+=1;
        else:
            print("something wrong")
    print("Popular percentage is: "+str(pop/len(label)*100));
    print("Unpopular percentage is: "+str(nonpop/len(label)*100));

def main():    
    col = input("Enter \"views\" or \"comments\": ");
    df = pd.read_csv("features_"+col+".csv")
    df.drop(df.columns[df.columns.str.contains('unnamed',case = False)],axis = 1, inplace = True)

    percentage_popular(df, col);

    for per in range(4, 9):
        total = 0;
        for i in range(0, 10):
            df = df.sample(frac=1).reset_index(drop=True)
            clf = train(df, per*.1, col);
            pair = test(df, per*.1, clf, col);
            total += pair[0]
            test_label, test_set = pair[1], pair[2]
            
        print("\nClassifier Accuracy is for "+str(per*.1)+": ");
        print(total/10);

    print_informative_features(df, clf, test_label, test_set);

if __name__== "__main__":
  main()