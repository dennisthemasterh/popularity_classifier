import pandas as pd;
from sklearn.naive_bayes import GaussianNB
import nltk
from sklearn.neighbors import KNeighborsClassifier
from sklearn import datasets

def train_nltk(df, fold, col):
    data = df.drop(columns = col);
    label = df[col];

    featuresets = [(data.loc[index], label.loc[index]) for index in df.index] 
    cutoff = int(len(featuresets)*fold);
    train_set = featuresets[:cutoff]
    
    classifier = nltk.NaiveBayesClassifier.train(train_set)
    return classifier;

def test_nltk(df, fold, classifier, col):
    data = df.drop(columns = col);
    label = df[col];
    featuresets = [(data.loc[index], label.loc[index]) for index in df.index] 
    cutoff = int(len(featuresets)*fold);
    test_set = featuresets[cutoff:]
    return nltk.classify.accuracy(classifier, test_set)

def train_gnb(df, fold, col):
    data = df.drop(columns = col);
    label = df[col];

    featuresets = data.copy(deep=True);
    cutoff = int(len(featuresets)*fold);
    
    train_set, train_label = featuresets[:cutoff], label[:cutoff]
    test_set, test_label = featuresets[cutoff:], label[cutoff:]

    clf = GaussianNB()
    clf.fit(train_set, train_label)
    return clf;

def test_gnb(df, fold, clf, col):
    data = df.drop(columns = col);
    label = df[col];
    featuresets = data.copy(deep=True);
    cutoff = int(len(featuresets)*fold);
    test_set, test_label = featuresets[cutoff:], label[cutoff:]
    return clf.score(test_set, test_label), test_label, test_set;

def train_knn(df, fold, num, col):
    data = df.drop(columns = col);
    label = df[col];

    featuresets = [data.loc[index] for index in df.index]#puts all into matrix. List of list
    cutoff = int(len(featuresets)*fold);
    train_set, train_label = featuresets[:cutoff], label[:cutoff]#get's some to train
    
    X = train_set
    y = train_label
    neigh = KNeighborsClassifier(n_neighbors=num)
    neigh.fit(X, y);
#    print(neigh.score(X, y));#just seeing how well it fits self
    return neigh;
    
def test_knn(df, fold, model, col):
    data = df.drop(columns = col);
    label = df[col];

    featuresets = [data.loc[index] for index in df.index]#puts all into matrix. List of list
    cutoff = int(len(featuresets)*fold);
    test_set, test_label = featuresets[cutoff:], label[cutoff:]#get's some to train
    
    X = test_set
    y = test_label
    score = model.score(X, y)
    
    column = {};
    return score, test_label, test_set;
    

def main():
    col = input("Enter \"views\" or \"comments\": ");
    df = pd.read_csv("features_"+col+".csv")
    #drops all except unigram count
    df.drop(df.columns[df.columns.str.contains('unnamed',case = False)],axis = 1, inplace = True)
    lst_col = ["duration", "num_speaker", "description_positivity", "title_positivity", "transcript_positivity", "job_activist", "job_advocate", "job_artist", "job_author", "job_biologist", "job_designer", "job_economist", "job_educator", "job_engineer", "job_entrepreneur", "job_expert", "job_inventor", "job_journalist", "job_other", "job_psychologist", "job_researcher", "job_scientist", "job_social", "job_writer", "tags_'art'", "tags_'biology'", "tags_'business'", "tags_'communication'", "tags_'creativity'", "tags_'culture'", "tags_'design'", "tags_'entertainment'", "tags_'future'", "tags_'global", "tags_'health'", "tags_'innovation'", "tags_'science'", "tags_'social", "tags_'society'", "tags_'technology'", "tags_change'", "tags_other", "description_abstractness", "title_abstractness", "script_abstractness"]
    df.drop(columns = lst_col,axis = 1, inplace = True)
    
    lst_nltk, lst_gnb, lst_knn = [], [], [];
    for per in range(4, 9):
        total_nltk, total_gnb, total_knn = 0, 0, 0;
        for i in range(0, 10):
            df = df.sample(frac=1).reset_index(drop=True)
            #for nltk
            clf = train_nltk(df, per*.1, col)
            total_nltk+= test_nltk(df, per*.1, clf, col);
            
            #for gnb
            clf = train_gnb(df, per*.1, col);
            pair = test_gnb(df, per*.1, clf, col);
            total_gnb += pair[0]
            
            #for knn
            model = train_knn(df, per*.1, 1, col);
            triple = test_knn(df, per*.1, model, col);
            total_knn+=triple[0];
            

        lst_nltk.append(total_nltk/10);
        lst_gnb.append(total_gnb/10);
        lst_knn.append(total_knn/10);

    print("For baseline models")
    print("For nltk naive bayes")
    print(lst_nltk)
    print("For gaussian naive bayes")
    print(lst_gnb)
    print("For knn")
    print(lst_knn)
    
if __name__== "__main__":
  main()