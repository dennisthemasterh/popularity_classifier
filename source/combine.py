import pandas as pd
import numpy as np;

#for combining files into one correctly
def add_more_column(df, new_df):
    for column in new_df:
        df[column]=new_df[column];
    
def combine_script(df, df_script):#have to match url because the creators a dumbass who cant write correctly
    count = 0;
    df["transcript"] = np.nan
    for row in df.index:
        url = df.loc[row]["url"]
        for script_row in df_script.index:
            if(url ==df_script.loc[script_row]["url"]):
                df["transcript"].loc[row]=df_script.loc[script_row]["transcript"]
                count+=1;
                break;
    return df;
    
def main():
    df = pd.read_csv("ted_main.csv");
    df_script = pd.read_csv("transcripts.csv");
    df = combine_script(df, df_script);
    df.to_csv("ted_w_script.csv");

if __name__ == "__main__":
    main()
