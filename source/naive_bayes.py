import pandas as pd;
import nltk

def train(df, fold, col):
    data = df.drop(columns = col);
    label = df[col];

    featuresets = [(data.loc[index], label.loc[index]) for index in df.index] 
    cutoff = int(len(featuresets)*fold);
    train_set = featuresets[:cutoff]
    
    classifier = nltk.NaiveBayesClassifier.train(train_set)
    return classifier;

def test(df, fold, classifier, col):
    data = df.drop(columns = col);
    label = df[col];
    featuresets = [(data.loc[index], label.loc[index]) for index in df.index] 
    cutoff = int(len(featuresets)*fold);
    test_set = featuresets[cutoff:]
    return nltk.classify.accuracy(classifier, test_set)
    
def percentage_popular(df, col):
    label = df[col];
    pop, nonpop = 0, 0;
    for index in label:
        if(index==1):
            pop+=1;
        elif(index==0):
            nonpop+=1;
        else:
            print("something wrong")
    print("Popular percentage is: "+str(pop/len(label)*100));
    print("Unpopular percentage is: "+str(nonpop/len(label)*100));
    
def main():    
    col = input("Enter \"views\" or \"comments\": ");
    df = pd.read_csv("features_"+col+".csv")
    df.drop(df.columns[df.columns.str.contains('unnamed',case = False)],axis = 1, inplace = True)

    percentage_popular(df, col);
    for per in range(4, 9):
        total = 0;
        for i in range(0, 10):
            df = df.sample(frac=1).reset_index(drop=True)
            clf = train(df, per*.1, col)
            total += test(df, per*.1, clf, col);
        print("Classifier Accuracy for "+str(per*.1)+": ");
        print(total/10);
    clf.show_most_informative_features(15);

if __name__== "__main__":
  main()
