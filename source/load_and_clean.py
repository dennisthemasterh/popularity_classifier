import pandas as pd
import numpy as np;
import re;
from collections import Counter;
from nltk.corpus import stopwords
from textblob import TextBlob

def add_more_column(df, new_df):#add column into dataframe
    for column in new_df:
        df[column]=new_df[column];
    
def correct_label(df, column, min, max):#convert column into 0 or 1
    lst_drop = [];
    for row in range(0, len(df)):
        value = df[column].iloc[row]
        if(value<min):#not popular
            df[column].iloc[row] = 0
        elif(value>max):#is popular
            df[column].iloc[row] = 1;
        else:
            lst_drop.append(row);    
    for index in lst_drop:
        df = df.drop([index]);
    return df;
    
def dummy_data(df):#dummy occupation and tags
    lst_occupation= [];
    lst_tags = [];
    for row in range(0, len(df)):#puts occupation and tags into list
        value = df["speaker_occupation"].iloc[row]
        tags = df["tags"].iloc[row];
        lst_occupation.extend([word.lower() for word in re.findall(r"[\w']+", value)])
        lst_tags.extend([word.lower() for word in re.findall(r"[\w']+", tags)])
    
    indicators = [];
    tags = [];#keeps top 20 most common
    for name in Counter(lst_occupation).most_common(20):#find most common items to dummy
        indicators.append(name[0])#gets the most popular occupation to dummy
    indicators.append("other")
    
    for name in Counter(lst_tags).most_common(20):#find most common tags to dummy
        tags.append(name[0])#gets the most popular tags to dummy
    tags.append("other")

    ans = []
    for pair in indicators:
        if(pair not in set(stopwords.words('english'))):#gets rid of all the stopwords
            ans.append(pair);
    indicators = ans;
    
    new_job = seperate_job_into_categories(df["speaker_occupation"], indicators)
    add_more_column(df, pd.get_dummies(new_job, prefix='job'))
    
    new_tag = seperate_tags_into_categories(df["tags"], tags)
    add_more_column(df, pd.get_dummies(new_tag, prefix='tags'))
    
    df = df.drop(columns = ["speaker_occupation", "tags"]);
    return df;

def seperate_tags_into_categories(tags, indicators):#for dummy tags
    ans = [];    
    for index in tags:#goes through dirty jobs
        bool = True;
        for tag in indicators:#goes through good job
            if tag in index:
                ans.append(tag);bool=False;break;
        if(bool):
            ans.append("other")#append other if not found job
    return ans;
        
def seperate_job_into_categories(occupation, indicators):#for dummy occupation
    ans = [];    
    for index in occupation:#goes through dirty jobs
        bool = True;
        for job in indicators:#goes through good job
            if job in [word.lower() for word in re.findall(r"[\w']+", index)]:
                ans.append(job);bool=False;break;
        if(bool):
            ans.append("other")#append other if not found job
    return ans;
        
def abstract_feature(dict, df):#convert words into abstract score
    lst_desciption_abstract, lst_title_abstract, lst_script_abstract=[], [], [];
    for row in df.index:
        lst_desciption_abstract.append(find_score(dict, df["description"].iloc[row].split(" ")));
        lst_title_abstract.append(find_score(dict, df["title"].iloc[row].split(" ")));
        lst_script_abstract.append(find_score(dict, df["transcript"].iloc[row].split(" ")));
    df['description_abstractness'] = pd.Series(lst_desciption_abstract, index=df.index)
    df['title_abstractness'] = pd.Series(lst_title_abstract, index=df.index)
    df['script_abstractness'] = pd.Series(lst_script_abstract, index=df.index)
    return df;
    
def find_score(dict, sentence):#find the abstractness   
    score, word_counted= 0, 0;
    for word in sentence:
        word = word.lower();#convert to lowercase words
        if word in dict:#if found
            score+=dict[word];
            word_counted +=1;
    if(word_counted==0):#no words found, return default average of three
        return 3;
    return score/word_counted;#return average score for words found

def adds_word_features(df, column, num):#change words into count vector
    new_df = pd.DataFrame(index = df.index)#to hold new columns
    lst_words = word_to_features(df, column, num);
    for word in lst_words:
        new_df[word+"_count"] = 0;#initialize columns
        
    for row in df.index:
        #to convert sentence into words
        important_words = []
        script = df.loc[row][column];
        words = [word.lower() for word in re.findall(r"[\w']+", script)]
        
        #count features in word list
        for token in lst_words:
            new_df.loc[row][token+"_count"] += words.count(token)
    add_more_column(df, new_df);
    return df;
    
def word_to_features(df, column, num = 20):#gets list of words to use as features
    lst_total, important_words, wordlst=[], [], [];
    #gets all words in the whole column
    for row in df.index:
        script = df.loc[row][column];
        words = [word.lower() for word in re.findall(r"[\w']+", script)]
        lst_total.extend(words);#gets list of all words
    #get rid of stop words
    for word in lst_total:
        if word not in stopwords.words('english'):
            important_words.append(word)
    #gets most common words
    for name in Counter(important_words).most_common(num):#find most common items to dummy
        wordlst.append(name[0])#gets the most popular occupation to dummy
    return wordlst;
    
def write_views(df):#convert views into boolean
    df = correct_label(df, "views", 673036, 2004519);#Activate for actual thing
    df = df.drop(columns = ["comments", "description", "title", "transcript"]);
    df.to_csv("features_views.csv");

def write_comments(df):#convert comments into boolean
    df = correct_label(df, "comments", 52, 265);#Activate for actual thing
    df = df.drop(columns = ["description", "title", "transcript", "views"]);
    df.to_csv("features_comments.csv");

def find_positivity(df):
    lst_desciption_positivity, lst_title_positivity, lst_transcript_positivity = [], [], []
    for row in df.index:
        lst_desciption_positivity.append(calculate_positivity(df["description"].iloc[row]));
        lst_title_positivity.append(calculate_positivity(df["title"].iloc[row]));
        lst_transcript_positivity.append(calculate_positivity(df["transcript"].iloc[row]));
        
    df['description_positivity'] = pd.Series(lst_desciption_positivity, index=df.index)
    df['title_positivity'] = pd.Series(lst_title_positivity, index=df.index)
    df['transcript_positivity'] = pd.Series(lst_transcript_positivity, index=df.index)
    return df;
    
def calculate_positivity(text):#finds text positivity
    word_list = [];
    for word in [word.lower() for word in re.findall(r"[\w']+", text)]:
        if word not in stopwords.words('english'):
            word_list.append(word)
    blob = TextBlob(' '.join(word for word in word_list))
    if(len(blob.sentences)==0):
        return 0;
    return blob.sentences[0].sentiment.polarity

def main():
    df = pd.read_csv("ted_w_script.csv");
    df = df.drop(columns = ["event", "film_date", "main_speaker", "name", "ratings", "related_talks", "url", "published_date", "languages"]);
    df = df.replace(np.nan, '', regex=True)

    df = dummy_data(df);
    df = find_positivity(df)
    
    concrete_df = pd.read_excel("Concreteness_ratings_Brysbaert_et_al_BRM.xlsx", sheet_name='Sheet1')
    dict = {};
    for row in concrete_df.index:
        dict[concrete_df.loc[row]["Word"]]= concrete_df.loc[row]["Conc.M"]
    df = abstract_feature(dict, df);

    df = adds_word_features(df, "description", 5);
    df = adds_word_features(df, "title", 2);
    df = adds_word_features(df, "transcript", 20);
    
    total_df = df.copy(deep=True)
    write_views(total_df);
    write_comments(total_df);
    
if __name__== "__main__":
  main()